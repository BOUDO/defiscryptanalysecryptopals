// #include <stdio.h>
// #include <stdlib.h>
#include "xorFix.h"
// #include <string.h>
// #include <time.h>


int main(int argc, char const *argv[])
{
	printf("\n************************ CONVERTIR EN BASE 64 ********************\n\n");
	char* chaine1 = "1c0111001f010100061a024b53535009181c";
	char* chaine2 = "686974207468652062756c6c277320657965";
	char* test = "746865206b696420646f6e277420706c6179";
		  //746865206B696420646F6e277420706C6179

	char* xor = xorFix(chaine1, chaine2);
	printf("chaine1 : %s\n", chaine1);
	printf("chaine2 : %s\n", chaine2);
	printf("xor : %s\n", xor);

	if(strcmp(xor, test) == 0)
	{
		printf("\nxorFix correcte\n");
	}
	printf("\n************************ FIN ********************\n");

	free(xor);


	return 0;
}