// #include <stdio.h>
// #include <stdlib.h>
#include "xorFix.h"
#include <string.h>


char* xorFix(char* chaine1, char* chaine2)
{
	if(strlen(chaine1) != strlen(chaine2))
	{
		printf("xorFix impossinle car 2 chaine de taille different\n");
		exit(40);
	}
	char* bits1 = NULL;
	char* bits2 = NULL;
	char* xor = NULL;
	char* hexa = NULL;
	int taille = 0;
	// char car1[2];
	// char car2[2];
	// car1[1] = '\0';
	// car2[1] = '\0';
	
	bits1 = convHexaBinaire(chaine1);
	printf("%s\n", bits1);
	bits2 = convHexaBinaire(chaine2);
	printf("%s\n", bits2);
	taille = strlen(bits1);
	xor = malloc(taille * sizeof(char) + sizeof(char));
	if(xor == NULL)
	{
		exit(40);
	}
	
	
	xor[taille] = '\0';

	for (int i = 0; i < taille; i++)
	{
		if(bits1[i] != bits2[i])
		{
			xor[i] = '1';
		}
		else
		{
			xor[i] = '0';
		}
		// car1[0] = bits1[i];
		// car2[0] = bits2[i];
		// // printf("car1 %s ", car1);
		// // printf("car2 %s\n", car2);
		// // printf("atoi car1 %d ", atoi(car1));
		// // printf("atoi car2 %d\n", atoi(car2));
		// xor[i] = ((atoi(car1) * atoi(car2) + 1)  % 2) + '0';
	}
	printf("%s\n", xor);
	hexa = convBinaireHexa(xor);
	free(bits1);
	free(bits2);
	free(xor);

	return hexa;
}