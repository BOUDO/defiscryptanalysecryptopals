#include <stdio.h>
#include <stdlib.h>
#include "convertion.h"
#include <string.h>
#include <time.h>



int convBaseXBase10(void* Base, char car);
{
	for (int i = 0; i < Base.taille; i++)
	{
		if(Base.valCodage[i] == car || 32 -  (Base.valCodage[i] - '0') == car - '0')
		{
			return i;
		}
	}
	return -1;
}

char* convBase10BaseX(void* Base, const int decimals[], int taille)
{
	printf("%d\n", taille);
	char* hexa = NULL;
	hexa = malloc(taille * sizeof(char) + sizeof(char));
	hexa[taille] = '\0';
	// printf("ok\n");
	for (int i = taille - 1; i >= 0; i--)
	{	
		// printf("%d\n", decimals[i]);
		hexa[i] = Base.valCodage[ decimals[i] ];
	}
	return hexa;
}

char* base10Binaire(int nbBit, int decimal)
{
	// printf("nbBit = %d\n", nbBit);
	char* bits = NULL;
	bits = malloc(nbBit * sizeof(char) + sizeof(char));
	if(bits == NULL)
	{
		exit(1);
	}
	int tmp = decimal;
	for (int j = 0; j < nbBit ; j++)
	{
		bits[j] = '0';
	}
	bits[nbBit] = '\0';
	int i = nbBit - 1;
	// printf("init %s\n", bits);
	while (tmp > 0)
	{
		if(tmp % 2 != 0)
		{
			bits[i] = '1'; 
		}
		
		tmp = tmp / 2;
		i--;
	}
	// printf("%ld\n", strlen(bits));
	// printf("%s\n", bits);
	return bits;
}

char* convBinaireDeBaseX(void* Base, char car)
{
	char* bits;
	int decimal;
	int nbBit = Base.nbBit;
	decimal = convBaseXBase10(Base, car);
	// printf("%d\n", decimal);
	bits = base10Binaire(nbBit, decimal);

	return bits;
}

char* convBaseXBinaire(void* Base, const char* hexa);
{
	int taille = strlen(hexa);
	char* bits = NULL;
	bits = malloc(Base.nbBit * taille * sizeof(char) + sizeof(char));
	if(bits == NULL)
	{
		exit(2);
	}
	bits[0] = '\0';
	char* tmp = NULL;

	for(int i = 0; i < taille; i++)
	{
		tmp = convBinaireDeBaseX(Base, hexa[i]);
		// printf("tmp = %s\n", tmp);
		strcat(bits, tmp);
		free(tmp);
	}

	return bits;
}


char* convBinaireBaseX(void* Base, const char* binaire);
{
	int taille = 0;
	int nbBit = Base.nbBit;
	double t = 0;
	int* decimals = NULL;
	char* hexa = NULL;
	decimals = convBinaireBase10(nbBit, binaire);
	t = strlen(binaire);
	// printf("%lf\n", t);
	taille = partIntSup(t/nbBit);
	// printf("bonnnn\n");
	hexa = convBase10BaseX(Base, decimals, taille);
	// printf("b10Hexa\n");
	free(decimals);
	return hexa;
}

int* convBinaireBase10(int nbBit, const char* binaire)
{
	double taille = strlen(binaire);
	int T = partIntSup(taille / nbBit);
	// printf("%d\n", T);
	int t = taille;
	// printf("%d\n", t);
	int* en64 = NULL;
	en64 =  malloc(T * sizeof(int));
	if(en64 == NULL || binaire == NULL)
	{
		exit(3);
	}
	int tmp = t - nbBit;
	char car[2];
	car[1] = '\0';
	int deuxf;
	int val;
	while(tmp >= 0)
	{
		deuxf = 2;
		car[0] = binaire[t - 1];
		// printf("%s ", car);
		val = atoi(car);
		for (int i = t - 2; i >= tmp; i--)
		{
			car[0] = binaire[i];
			// printf("%s ", car);
			val += atoi(car) * deuxf;
			deuxf *= 2;
		}
		T--;
		en64[T] = val;
		// printf("T = %d, val = %d\n", T, val);
		t = tmp;
		tmp = t - nbBit;
	}
	if(t > 0)
	{
		val = 0;
		deuxf = 1;
		// printf("ici\n");
		// printf("%d\n", t);
		for (int j = t-1;  j >= 0; j--)
		{
			car[0] = binaire[j];
			val += atoi(car) * deuxf;
			deuxf *= 2;
		}
		en64[0] = val;
	}
	// printf("Fin base10\n");
	return en64;
}

char* convBaseXBasY(void* BaseX, void* BaseY, const char* hexa);
{
	int taille = 0;
	int nbBit = 6;
	double t = 0;
	char* bits = NULL;
	int* decimals = NULL;
	char* conv = NULL;
	if(hexa == NULL)
	{
		exit(10);
	}

	bits = convBaseXBinaire(BaseX, hexa);
	if(bits == NULL)
	{
		exit(20);
	}
	decimals = convBinaireBase10(BaseY.nbBit, bits);
	if(decimals == NULL)
	{
		exit(30);
	}
	t = strlen(bits);
	taille = partIntSup(t / nbBit);
	conv = convBase10BaseX(Base, decimals, taille);

	free(bits);
	free(decimals);
	return conv;
}

int partIntSup(double x)
{
	int tmp = x;
	double d = x - tmp;

	if(d > 0)
	{
		return tmp + 1;
	}
	return tmp;
}