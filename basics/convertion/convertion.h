#ifndef BASE216
#define BASE16


int convBaseXBase10(void* Base, char car);

char* convBase10BaseX(void* Base, const int decimals[], int taille);

char* base10Binaire(int nbBit, int decimal);

int* convBinaireBase10(int nbBit, const char* binaire);

char* convBinaireDeBaseX(void* Base, char car);

char* convBinaireBaseX(void* Base, const char* binaire);

char* convBaseXBinaire(void* Base, const char* hexa);

char* convBaseXBasY(void* BaseX, void* BaseY, const char* hexa);

int partIntSup(double x);

#endif