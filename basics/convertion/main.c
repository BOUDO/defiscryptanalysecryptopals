
#include "hexaToBase64.h"
#include <time.h>



int main(int argc, char const *argv[])
{
	Base64 b;
	Base16 bb;
	b = creeBase64();
	bb = creeBase16();
	char* ff = convHexaBinaire("0123456789");
	printf("%s\n", b.valCodage);
	printf("%s\n", bb.valCodage);
	printf("%s\n", ff);
	double x = 0.215645;
	double y,z;
	y = 5; z = 3;
	printf("partIntSup(%f) = %d\n",x ,partIntSup(y/z));

	printf("\n************************ CONVERTIR EN BASE 64 ********************\n\n");
	char* conv = NULL;
	char* hexa = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";	
	char* test = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
			  // "ASdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	conv = convHexaBase64(hexa);
	printf("Hexadecimal : %s\n", hexa);
	printf("Base 64 : %s\n", conv);

	if(strcmp(conv, test) == 0)
	{
		printf("\nConversion correcte\n");
	}
	printf("\n************************ FIN ********************\n");

	// printf("\n************************ CONVERTIR EN BASE 64 ********************\n\n");
	// char* chaine1 = "1c0111001f010100061a024b53535009181c";
	// char* chaine2 = "686974207468652062756c6c277320657965";
	// test = "746865206b696420646f6e277420706c6179";
	// 	  //746865206B696420646F6e277420706C6179

	// char* xor = xorFix(chaine1, chaine2);
	// printf("chaine1 : %s\n", chaine1);
	// printf("chaine2 : %s\n", chaine2);
	// printf("xor : %s\n", xor);

	// if(strcmp(xor, test) == 0)
	// {
	// 	printf("\nxorFix correcte\n");
	// }
	// printf("\n************************ FIN ********************\n");


	free(ff);
	free(conv);
	// free(xor);

	return 0;
}