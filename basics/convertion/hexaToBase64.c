#include "hexaToBase64.h"


Base64 creeBase64()
{
	Base64 b;
	int majuscule = 65;
	int minuscule = 97;
	int chiffre = 48;
	for (int i = 0; i < 26; i++)
	{
		b.valCodage[i] = majuscule;
		majuscule++;
	}
	for (int j = 26; j < 52; j++)
	{
		b.valCodage[j] = minuscule;
		minuscule++;
	}
	for (int k = 52; k < 62; k++)
	{
		//printf("%c\n", chiffre);
		b.valCodage[k] = chiffre;
		chiffre++;
	}

	b.valCodage[62] = '+';
	b.valCodage[63] = '/';
	b.valCodage[64] = '\0';
	b.taille = 64;
	b.nbBit = 6;

	return b;
}

Base16 creeBase16()
{	
	Base16 b;
	char chiffre = 48;
	char car = 97;
	for(int i = 0; i < 10; i++)
	{
		b.valCodage[i] = chiffre;
		chiffre++;
	}
	for(int j = 10; j < 16; j++)
	{
		b.valCodage[j] = car;
		car++;
	}
	b.valCodage[16] = '\0';
	b.taille = 16;
	b.nbBit = 4;

	return b;
}

int convHexaBase10(char car)
{
	Base16 b = creeBase16();
	for (int i = 0; i < b.taille; ++i)
	{
		if(b.valCodage[i] == car || 32 -  (b.valCodage[i] - '0') == car - '0')
		{
			return i;
		}
	}
	return -1;
}

char* base10Binaire(int nbBit, int decimal)
{
	// printf("nbBit = %d\n", nbBit);
	char* bits = NULL;
	bits = malloc(nbBit * sizeof(char) + sizeof(char));
	if(bits == NULL)
	{
		exit(1);
	}
	int tmp = decimal;
	for (int j = 0; j < nbBit ; j++)
	{
		bits[j] = '0';
	}
	bits[nbBit] = '\0';
	int i = nbBit - 1;
	// printf("init %s\n", bits);
	while (tmp > 0)
	{
		if(tmp % 2 != 0)
		{
			bits[i] = '1'; 
		}
		
		tmp = tmp / 2;
		i--;
	}
	// printf("%ld\n", strlen(bits));
	// printf("%s\n", bits);
	return bits;
}

char* getBinaireDeHexa(char car)
{
	char* bits;
	int decimal;
	int nbBit = 4;
	decimal = convHexaBase10(car);
	// printf("%d\n", decimal);
	bits = base10Binaire(nbBit, decimal);

	return bits;
}

char* convHexaBinaire(const char* hexa)
{
	int taille = strlen(hexa);
	char* bits = NULL;
	bits = malloc(4 * taille * sizeof(char) + sizeof(char));
	if(bits == NULL)
	{
		exit(2);
	}
	bits[0] = '\0';
	char* tmp = NULL;

	for(int i = 0; i < taille; i++)
	{
		tmp = getBinaireDeHexa(hexa[i]);
		// printf("tmp = %s\n", tmp);
		strcat(bits, tmp);
		free(tmp);
	}

	return bits;
}

int partIntSup(double x)
{
	int tmp = x;
	double d = x - tmp;

	if(d > 0)
	{
		return tmp + 1;
	}
	return tmp;
}

int* convBinaireBase10(int nbBit, const char* binaire)
{
	double taille = strlen(binaire);
	int T = partIntSup(taille / nbBit);
	// printf("%d\n", T);
	int t = taille;
	// printf("%d\n", t);
	int* en64 = NULL;
	en64 =  malloc(T * sizeof(int));
	if(en64 == NULL || binaire == NULL)
	{
		exit(3);
	}
	int tmp = t - nbBit;
	char car[2];
	car[1] = '\0';
	int deuxf;
	int val;
	while(tmp >= 0)
	{
		deuxf = 2;
		car[0] = binaire[t - 1];
		// printf("%s ", car);
		val = atoi(car);
		for (int i = t - 2; i >= tmp; i--)
		{
			car[0] = binaire[i];
			// printf("%s ", car);
			val += atoi(car) * deuxf;
			deuxf *= 2;
		}
		T--;
		en64[T] = val;
		// printf("T = %d, val = %d\n", T, val);
		t = tmp;
		tmp = t - nbBit;
	}
	if(t > 0)
	{
		val = 0;
		deuxf = 1;
		// printf("ici\n");
		// printf("%d\n", t);
		for (int j = t-1;  j >= 0; j--)
		{
			car[0] = binaire[j];
			val += atoi(car) * deuxf;
			deuxf *= 2;
		}
		en64[0] = val;
	}
	// printf("Fin base10\n");
	return en64;
}

char* convBase10Base64(const int decimals[], int taille)
{
	Base64 b = creeBase64();
	char* enBase64 = NULL;
	enBase64 =  malloc(taille * sizeof(char) + sizeof(char));
	if(enBase64 == NULL || decimals == NULL)
	{
		exit(4);
	}
	enBase64[taille] = '\0';

	for (int i = taille - 1; i >= 0 ; i--)
	{
		enBase64[i] = b.valCodage[ decimals[i] ];
	}
	return enBase64;
}

char* convHexaBase64(const char* hexa)
{
	int taille = 0;
	int nbBit = 6;
	double t = 0;
	char* bits = NULL;
	int* decimals = NULL;
	char* conv = NULL;
	if(hexa == NULL)
	{
		exit(10);
	}

	bits = convHexaBinaire(hexa);
	if(bits == NULL)
	{
		exit(20);
	}
	decimals = convBinaireBase10(nbBit, bits);
	if(decimals == NULL)
	{
		exit(30);
	}
	t = strlen(bits);
	taille = partIntSup(t / nbBit);
	conv = convBase10Base64(decimals, taille);

	free(bits);
	free(decimals);
	return conv;
}

char* convBase10Hexa(const int decimals[], int taille)
{
	printf("%d\n", taille);
	Base16 b = creeBase16();
	char* hexa = NULL;
	hexa = malloc(taille * sizeof(char) + sizeof(char));
	hexa[taille] = '\0';
	// printf("ok\n");
	for (int i = taille - 1; i >= 0; i--)
	{	
		// printf("%d\n", decimals[i]);
		hexa[i] = b.valCodage[ decimals[i] ];
	}
	return hexa;
}

char* convBinaireHexa(const char* binaire)
{
	int taille = 0;
	int nbBit = 4;
	double t = 0;
	int* decimals = NULL;
	char* hexa = NULL;
	decimals = convBinaireBase10(nbBit, binaire);
	t = strlen(binaire);
	// printf("%lf\n", t);
	taille = partIntSup(t/nbBit);
	// printf("bonnnn\n");
	hexa = convBase10Hexa(decimals, taille);
	// printf("b10Hexa\n");
	free(decimals);
	return hexa;
}

// char* xorFix(char* chaine1, char* chaine2)
// {
// 	if(strlen(chaine1) != strlen(chaine2))
// 	{
// 		printf("xorFix impossinle car 2 chaine de taille different\n");
// 		exit(40);
// 	}
// 	char* bits1 = NULL;
// 	char* bits2 = NULL;
// 	char* xor = NULL;
// 	char* hexa = NULL;
// 	int taille = 0;
// 	// char car1[2];
// 	// char car2[2];
// 	// car1[1] = '\0';
// 	// car2[1] = '\0';
	
// 	bits1 = convHexaBinaire(chaine1);
// 	printf("%s\n", bits1);
// 	bits2 = convHexaBinaire(chaine2);
// 	printf("%s\n", bits2);
// 	taille = strlen(bits1);
// 	xor = malloc(taille * sizeof(char) + sizeof(char));
// 	if(xor == NULL)
// 	{
// 		exit(40);
// 	}
	
	
// 	xor[taille] = '\0';

// 	for (int i = 0; i < taille; i++)
// 	{
// 		if(bits1[i] != bits2[i])
// 		{
// 			xor[i] = '1';
// 		}
// 		else
// 		{
// 			xor[i] = '0';
// 		}
// 		// car1[0] = bits1[i];
// 		// car2[0] = bits2[i];
// 		// // printf("car1 %s ", car1);
// 		// // printf("car2 %s\n", car2);
// 		// // printf("atoi car1 %d ", atoi(car1));
// 		// // printf("atoi car2 %d\n", atoi(car2));
// 		// xor[i] = ((atoi(car1) * atoi(car2) + 1)  % 2) + '0';
// 	}
// 	printf("%s\n", xor);
// 	hexa = convBinaireHexa(xor);
// 	free(bits1);
// 	free(bits2);
// 	free(xor);

// 	return hexa;
// }




// int main(int argc, char const *argv[])
// {
// 	Base64 b;
// 	Base16 bb;
// 	b = creeBase64();
// 	bb = creeBase16();
// 	char* ff = convHexaBinaire("0123456789");
// 	printf("%s\n", b.valCodage);
// 	printf("%s\n", bb.valCodage);
// 	printf("%s\n", ff);
// 	double x = 0.215645;
// 	double y,z;
// 	y = 5; z = 3;
// 	printf("partIntSup(%f) = %d\n",x ,partIntSup(y/z));

// 	printf("\n************************ CONVERTIR EN BASE 64 ********************\n\n");
// 	char* conv = NULL;
// 	char* hexa = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";	
// 	char* test = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
// 			  // "ASdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
// 	conv = convHexaBase64(hexa);
// 	printf("Hexadecimal : %s\n", hexa);
// 	printf("Base 64 : %s\n", conv);

// 	if(strcmp(conv, test) == 0)
// 	{
// 		printf("\nConversion correcte\n");
// 	}
// 	printf("\n************************ FIN ********************\n");

// 	printf("\n************************ CONVERTIR EN BASE 64 ********************\n\n");
// 	char* chaine1 = "1c0111001f010100061a024b53535009181c";
// 	char* chaine2 = "686974207468652062756c6c277320657965";
// 	test = "746865206b696420646f6e277420706c6179";
// 		  //746865206B696420646F6e277420706C6179

// 	char* xor = xorFix(chaine1, chaine2);
// 	printf("chaine1 : %s\n", chaine1);
// 	printf("chaine2 : %s\n", chaine2);
// 	printf("xor : %s\n", xor);

// 	if(strcmp(xor, test) == 0)
// 	{
// 		printf("\nxorFix correcte\n");
// 	}
// 	printf("\n************************ FIN ********************\n");


// 	free(ff);
// 	free(conv);
// 	free(xor);

// 	return 0;
// }