#ifndef HEXATOBASE24
#define HEXATOBASE24

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct base64
{
	char valCodage[65];
	int taille;
	int nbBit;
};
typedef struct base64 Base64;

struct hexadecimal
{
	char valCodage[17];
	int taille;
	int nbBit;
};
typedef struct hexadecimal Base16;

Base64 creeBase64();

Base16 creeBase16();

int convHexaBase10(char car);

char* base10Binaire(int nbBit, int decimal);

char* getBinaireDeHexa(char car);

char* convHexaBinaire(const char* hexa);

int partIntSup(double x);

int* convBinaireBase10(int nbBit, const char* binaire);

char* convBase10Base64(const int decimals[], int taille);

char* convHexaBase64(const char* hexa);

char* convBase10Hexa(const int decimals[], int taille);

char* convBinaireHexa(const char* binaire);

// char* xorFix(char* chaine1, char* chaine2);


char* getBinaire(char car);


#endif